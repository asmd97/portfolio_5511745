import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:portfolio_5511745/info.dart';
import 'package:portfolio_5511745/lebenslauf.dart';
import 'package:portfolio_5511745/projects.dart';
import 'package:portfolio_5511745/skils.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(Portfolio_5511745());
}

class Portfolio_5511745 extends StatelessWidget {
  Portfolio_5511745({super.key});

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: FutureBuilder(
            future: _initialization,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                print("Error");
              }
              if (snapshot.connectionState == ConnectionState.done) {
                return MyHomePage();
              }
              return CircularProgressIndicator();
            }));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  MyHomePageState createState() => MyHomePageState();
}

class MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Color.fromARGB(255, 2, 50, 89),
        appBar: AppBar(
          title: const Text(
            "Startseite",
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          backgroundColor: const Color.fromARGB(255, 2, 50, 89),
          iconTheme: const IconThemeData(color: Colors.white),
        ),
        drawer: const NavigationDrawer(),

        //drawerScrimColor: Color.fromARGB(255, 11, 3, 164),
        // Wechsel der Hintergrund bei der öffnen von Drawer

        body: const Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Willkommen zu meinem Portfolio!',
                style: TextStyle(fontSize: 70, color: Colors.white),
              ),
              Text(
                'Hier können Sie mehr über meine berufliche Reise',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              Text(
                'meine Fähigkeiten und die Projekte, an denen ich gearbeitet habe erfahren',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
              Text(
                'Lassen Sie uns gemeinsam die spannenden Details meiner Karriere entdecken.',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            ],
          ),
        ),
      );
}

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Drawer(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildHeader(context),
              buildMenuItems(context),
            ],
          ),
        ),
      );

  Widget buildHeader(BuildContext context) => Container(
        color: Colors.red.shade300,
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).padding.top,
        ),
        child: Column(children: [
          UserAccountsDrawerHeader(
            accountName: const Text('Amir Samadi',
                style: TextStyle(color: Colors.black)),
            accountEmail: const Text('amir.samadi@mnd.thm.de',
                style: TextStyle(color: Colors.black)),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                  child: Image.asset('Image5.jpg', fit: BoxFit.scaleDown)),
            ),
            decoration: const BoxDecoration(
                color: Colors.blue,
                image: DecorationImage(
                    image: AssetImage('Image5.jpg'), fit: BoxFit.cover)),
          )
        ]),
      );

  Widget buildMenuItems(BuildContext context) => Container(
        padding: const EdgeInsets.all(24),
        color: const Color.fromARGB(255, 2, 50, 89),
        child: Wrap(
          runSpacing: 16,
          children: [
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.home_outlined),
                title: const Text('Startseite'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => Portfolio_5511745()));
                }),
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.info_outlined),
                title: const Text('Über mich'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Info(),
                  ));
                }),
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.school_outlined),
                title: const Text('Lebenslauf'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Lebenslauf(),
                  ));
                }),
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.work_outlined),
                title: const Text('Fähigkeiten'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Skils(),
                  ));
                }),
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.group),
                title: const Text('Projekte'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Projects(),
                  ));
                }),
            const Divider(color: Colors.black54),
            /*
            ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.settings),
                title: const Text('Settings'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Settings(),
                  ));
                }),
             ListTile(
                iconColor: Colors.white,
                textColor: Colors.white,
                leading: const Icon(Icons.logout),
                title: const Text('Sign Out'),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Signout(),
                  ));
                }),

*/
          ],
        ),
      );
}
